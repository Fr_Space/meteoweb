function Go() {
    $(document).ready(function() {
        const val_i_lieu = $("#i_lieu").val();
        if (val_i_lieu.length >= 2) {
            $("#b_coor").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé les ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>Latitude : ${data.coord.lat}</br>Longitude : ${data.coord.lon}</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_desc").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.weather[0].description}</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_temp").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.main.temp} °C</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_pres").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.main.pressure} Pa</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_humi").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.main.humidity} φ</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_temp_min").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.main.temp_min} °C</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_temp_max").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.main.temp_max} °C</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_visi").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.visibility} m</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_vent").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.wind.speed} KM/h</br>${data.wind.deg} °</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_pays").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $.getJSON(`https://restcountries.eu/rest/v2/alpha/${data.sys.country}`, function(country) {
                        $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${country.name}</span>`);
                        $("#i_lieu").val("");
                    });
                });
            });
            $("#b_nom_ville").click(function(event) {
                $.getJSON(`http://api.openweathermap.org/data/2.5/weather?q=${val_i_lieu}&units=metric&lang=fr&appid=a9d8c93ffedcd5e0b500f2e255fd92de`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé la ${event.currentTarget.innerText} à ${val_i_lieu} ?</span></br><span>${event.currentTarget.innerText} à ${val_i_lieu} :</br>${data.name}</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_code_pays").click(function(event) {
                $.getJSON(`https://restcountries.eu/rest/v2/name/${val_i_lieu}?fullText=true`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé le nom du pays ayant le code "${event.currentTarget.innerText}"?</span></br><span>${val_i_lieu} = ${data[0].alpha2Code}</span>`);
                    $("#i_lieu").val("");
                });
            });
            $("#b_nom_pays").click(function(event) {
                $.getJSON(`https://restcountries.eu/rest/v2/alpha/${val_i_lieu}`, function(data) {
                    $("#text_bot").html(`<span>Vous m'avez demandé le code du pays ayant comme nom "${event.currentTarget.innerText}"?</span></br><span>${val_i_lieu} = ${data.name}</span>`);
                    $("#i_lieu").val("");
                });
            });
        } else {
            $("#text_bot").html(`Vieulliez remplir le champ "Lieux" !`)
            $("#i_lieu").val("");
        }
    })
};